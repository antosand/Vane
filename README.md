## Instructions on how to run the application:

1. Clone this repository
2. Install **cocoapods** on your OSX machine
3. Within the project folder run `pod install` in your terminal
4. Build the application using XCode (**Command + B**)
5. Select a device or device simulator and press **Run** in the top left corner